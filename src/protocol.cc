/*
 * This file is part of the "rvlprog" software.
 *
 * Copyright (C) 2020, Angelo Dureghello
 * Copyright (C) 2020, Reveltronics - provided protocol
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the
 * Free Software Foundation
 * 51 Franklin Street, Fifth Floor
 * Boston, MA  02110-1301, USA.
 *
 * Note: all the code in this project respects uniform rules as in
 *   https://www.kernel.org/doc/html/v4.10/process/coding-style.html
 */

#include "protocol.hh"
#include "trace.hh"

#include <cstring>

enum message_types {
	msg_t_command = 1,
	msg_t_status,
	msg_t_data,
	msg_t_fw_version,
	msg_t_config,
	msg_t_custom_script_cfg,
	msg_t_custom_script_cfg_dly,
	msg_t_custom_script_instr,
	msg_t_sfpxfp_bruteforce,
};

enum command_types {
	cmd_set_hw_vpp = 0xf1,
	cmd_set_hw_bus,
	cmd_read_fw_ver,
	cmd_stop_operations = 0xf5,
	cmd_vpp_problem,
	cmd_read_mem = 0x11,
	cmd_write_mem = 0x21,
	cmd_clear_mem = 0x31,
	cmd_custom_script_set = 0x41,
	cmd_custom_script_exec,
	cmd_custom_script_reset,
	cmd_custom_nand_rd_blocks = 0x50,
	cmd_send_buff_start = 0xb1,
	cmd_send_buff_stop,
};

enum status_types {
	sta_t_none,
	sta_t_ok = 0x11,
	sta_t_wait,
	sta_t_idle,
	sta_t_synchropc,
	sta_t_progressprocent,
	sta_t_sfp_bruteforce,
	sta_t_abortuser = 0x21,
	sta_t_timeout,
	sta_t_voltage_problem,
	sta_t_noterased,
	sta_t_false = 0xEE,
};

enum mem_id {
	ee_gen_24x16 = 0x0005,
	ee_gen_24x32 = 0x0006,
	ee_gen_24x64 = 0x0007,
	ee_gen_24x128 = 0x0008,
	ee_gen_24x256 = 0x0009,
	ee_gen_24x512 = 0x000a,
	flash_sst_25vf010 = 0x0049,
	flash_sst_25vf010a = 0x004a,
	flash_gen_25x05 = 0x01e0,
	flash_gen_25x010 = 0x01f4,
	flash_gen_25x020 = 0x0208,
	flash_gen_25x040 = 0x0226,
	flash_gen_25x080 = 0x0244,
	flash_gen_25x016 = 0x0262,
	flash_gen_25x064 = 0x02b2,
	flash_gen_25x032 = 0x028a,
	flash_gen_25x128 = 0x02d0,
	flash_winbond_w25q80dv = 0x01ca,
	flash_winbond_w25q32jv = 0x0105,
	flash_winbond_w25q64jv = 0x0104,
	flash_winbond_w25q128fw = 0x0102,
};

enum mem_types {
	type_ee = 1,
	type_flash,
	type_fram,
};

enum mem_series {
	ms_24 = 1,
	ms_25,
	ms_93,
	ms_95,
	ms_45,
	ms_35,
	ms_sfpxfp,
	ms_26,
	ms_90,
	ms_spi_nand,
	ms_spi_nand_2,
	ms_ds,
};

enum vpp {
	vpp_0,
	vpp1v8 = 18,
	vpp2v5 = 25,
	vpp2v8 = 28,
	vpp3v0 = 30,
	vpp3v3 = 33,
	vpp5v0 = 50
};

enum bus {
	bus_none,
	bus_i2c,
	bus_spi,
	bus_uwire,
	bus_spi_atmel,
};

enum nand_dies {
	dies_1,
	dies_2,
};

constexpr struct target_info e_gen_24x16 = {
	{
		ee_gen_24x16, type_ee, ms_24,
		0x05, 0x0b, 0x08, 0x02, 0x04, 0x01, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		vpp5v0, bus_i2c,
		0x00, 0x02, 0x00, 0x00
	},
	1024 * 2, 256
};

constexpr struct target_info e_gen_24x32 = {
	{
		ee_gen_24x32, type_ee, ms_24,
		0x06, 0x0c, 0x0c, 0x02, 0x05, 0x02, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		vpp5v0, bus_i2c,
		0x00, 0x02, 0x00, 0x00
	},
	1024 * 4, 256
};

constexpr struct target_info e_gen_24x64 = {
	{
		ee_gen_24x64, type_ee, ms_24,
		0x07, 0x0d, 0x0d, 0x02, 0x05, 0x02, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		vpp5v0, bus_i2c,
		0x00, 0x02, 0x00, 0x00
	},
	1024 * 8, 256
};

constexpr struct target_info e_gen_24x128 = {
	{
		ee_gen_24x128, type_ee, ms_24,
		0x08, 0x0e, 0x0e, 0x02, 0x06, 0x02, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		vpp5v0, bus_i2c,
		0x00, 0x02, 0x00, 0x00
	},
	1024 * 16, 256
};

constexpr struct target_info e_gen_24x256 = {
	{
		ee_gen_24x256, type_ee, ms_24,
		0x09, 0x0f, 0x0f, 0x02, 0x06, 0x02, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		vpp5v0, bus_i2c,
		0x00, 0x02, 0x00, 0x00
	},
	1024 * 32, 256
};

constexpr struct target_info e_gen_24x512 = {
	{
		ee_gen_24x512, type_ee, ms_24,
		0x0a, 0x10, 0x10, 0x02, 0x07, 0x02, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		vpp5v0, bus_i2c,
		0x00, 0x02, 0x00, 0x00
	},
	1024 * 64, 256
};

constexpr struct target_info f_winbond_w25q128fw = {
	{
		flash_winbond_w25q128fw, type_flash, ms_25,
		0x4d, 0x18, 0x10, 0x00, 0x08, 0x00, 0x03, 0x03,
		0x02, 0x06, 0x50, 0x01, 0x05, 0xd8, 0x02,
		vpp1v8, bus_spi,
		0x00, 0x02, 0x00, 0x00
	},
	1024 * 1024 * 16, 16384
};

constexpr struct target_info f_winbond_w25q64jv = {
	{
		flash_winbond_w25q64jv, type_flash, ms_25,
		0x41, 0x17, 0x10, 0x00, 0x08, 0x00, 0x03, 0x03,
		0x02, 0x06, 0x50, 0x01, 0x05, 0xd8, 0x02,
		vpp3v0, bus_spi,
		0x00, 0x02, 0x00, 0x00
	},
	1024 * 1024 * 8, 16384
};

constexpr struct target_info f_winbond_w25q32jv = {
	{
		flash_winbond_w25q32jv, type_flash, ms_25,
		0x3f, 0x16, 0x00, 0x00, 0x08, 0x00, 0x03, 0x03,
		0x02, 0x06, 0x50, 0x01, 0x05, 0xc7, 0x02,
		vpp3v0, bus_spi,
		0x00, 0x02, 0x00, 0x00
	},
	1024 * 1024 * 4, 16384
};

constexpr struct target_info f_winbond_w25q80dv = {
	{
		flash_winbond_w25q80dv, type_flash, ms_25,
		0x49, 0x14, 0x00, 0x00, 0x08, 0x00, 0x03, 0x03,
		0x02, 0x06, 0x50, 0x01, 0x05, 0xc7, 0x02,
		vpp3v0, bus_spi,
		0x00, 0x02, 0x00, 0x00
	},
	1024 * 1024 * 1, 16384
};

constexpr struct target_info f_gen_25vf010 = {
	{
		flash_sst_25vf010, type_flash, ms_25,
		0x0e, 0x11, 0x00, 0x00, 0x00, 0x00, 0x00, 0x03,
		0x02, 0x06, 0x50, 0x01, 0x05, 0xc7, 0x42,
		vpp3v3, bus_spi,
		0x00, 0x02, 0x00, 0x00
	},
	1024 * 128, 4096
};

constexpr struct target_info f_gen_25vf010a = {
	{
		flash_sst_25vf010a, type_flash, ms_25,
		0x31, 0x11, 0x00, 0x00, 0x00, 0x00, 0x00, 0x03,
		0xaf, 0x06, 0x50, 0x01, 0x05, 0xc7, 0x42,
		vpp3v3, bus_spi,
		0x00, 0x02, 0x00, 0x00
	},
	1024 * 128, 4096
};

constexpr struct target_info f_gen_25x05 = {
	{
		flash_gen_25x05, type_flash, ms_25,
		0x00, 0x10, 0x00, 0x00, 0x00, 0x00, 0x00, 0x03,
		0x02, 0x06, 0x50, 0x01, 0x05, 0xc7, 0x42,
		vpp2v8, bus_spi,
		0x00, 0x02, 0x00, 0x00
	},
	1024 * 64, 2048
};

constexpr struct target_info f_gen_25x010 = {
	{
		flash_gen_25x010, type_flash, ms_25,
		0x0e, 0x11, 0x00, 0x00, 0x00, 0x00, 0x00, 0x03,
		0x02, 0x06, 0x50, 0x01, 0x05, 0xc7, 0x42,
		vpp2v8, bus_spi,
		0x00, 0x02, 0x00, 0x00
	},
	1024 * 128, 4096
};

constexpr struct target_info f_gen_25x020 = {
	{
		flash_gen_25x020, type_flash, ms_25,
		0x0f, 0x12, 0x00, 0x00, 0x00, 0x00, 0x00, 0x03,
		0x02, 0x06, 0x50, 0x01, 0x05, 0xc7, 0x42,
		vpp2v8, bus_spi,
		0x00, 0x02, 0x00, 0x00
	},
	1024 * 256, 4096
};

constexpr struct target_info f_gen_25x040 = {
	{
		flash_gen_25x040, type_flash, ms_25,
		0x10, 0x13, 0x00, 0x00, 0x00, 0x00, 0x00, 0x03,
		0x02, 0x06, 0x50, 0x01, 0x05, 0xc7, 0x42,
		vpp2v8, bus_spi,
		0x00, 0x02, 0x00, 0x00
	},
	1024 * 512, 8192
};

constexpr struct target_info f_gen_25x080 = {
	{
		flash_gen_25x080, type_flash, ms_25,
		0x11, 0x14, 0x00, 0x00, 0x00, 0x00, 0x00, 0x03,
		0x02, 0x06, 0x50, 0x01, 0x05, 0xc7, 0x42,
		vpp2v8, bus_spi,
		0x00, 0x02, 0x00, 0x00
	},
	1024 * 1024 * 1, 16384
};

constexpr struct target_info f_gen_25x016 = {
	{
		flash_gen_25x016, type_flash, ms_25,
		0x38, 0x15, 0x00, 0x00, 0x00, 0x00, 0x00, 0x03,
		0x02, 0x06, 0x50, 0x01, 0x05, 0xc7, 0x42,
		vpp2v8, bus_spi,
		0x00, 0x02, 0x00, 0x00
	},
	1024 * 1024 * 2, 16384
};

constexpr struct target_info f_gen_25x032 = {
	{
		flash_gen_25x032, type_flash, ms_25,
		0x3c, 0x16, 0x00, 0x00, 0x00, 0x00, 0x00, 0x03,
		0x02, 0x06, 0x50, 0x01, 0x05, 0xc7, 0x42,
		vpp2v8, bus_spi,
		0x00, 0x02, 0x00, 0x00
	},
	1024 * 1024 * 4, 16384
};

constexpr struct target_info f_gen_25x64 = {
	{
		flash_gen_25x064, type_flash, ms_25,
		0x40, 0x17, 0x00, 0x00, 0x00, 0x00, 0x00, 0x03,
		0x02, 0x06, 0x50, 0x01, 0x05, 0xc7, 0x42,
		vpp2v8, bus_spi,
		0x00, 0x02, 0x00, 0x00
	},
	1024 * 1024 * 8, 16384
};

constexpr struct target_info f_gen_25x128 = {
	{
		flash_gen_25x128, type_flash, ms_25,
		0x4d, 0x18, 0x00, 0x00, 0x08, 0x00, 0x03, 0x03,
		0x02, 0x06, 0x50, 0x01, 0x05, 0xc7, 0x02,
		vpp2v8, bus_spi,
		0x00, 0x02, 0x00, 0x00
	},
	1024 * 1024 * 16, 16384
};

/*
 * populating map
 */
void dev_db::setup_device_list()
{
	/* supported device list
 	 * keeping in only rwe tested stuff
	 */
	/* u  = added but untested, t = tested */
	dlist["ee_24..16"] = &e_gen_24x16;		/* u */
	dlist["ee_24..32"] = &e_gen_24x32;		/* u */
	dlist["ee_24..64"] = &e_gen_24x64;		/* u */
	dlist["ee_24..128"] = &e_gen_24x128;		/* u */
	dlist["ee_24..256"] = &e_gen_24x256;		/* t */
	dlist["ee_24..512"] = &e_gen_24x512;		/* u */
	dlist["fl_w25q32jv"] = &f_winbond_w25q32jv;	/* t */
	dlist["fl_w25q64jv"] = &f_winbond_w25q64jv;	/* u */
	dlist["fl_w25q80dv"] = &f_winbond_w25q80dv;	/* t */
	dlist["fl_w25q128fw"] = &f_winbond_w25q128fw;	/* t */
	dlist["fl_25vf010"] = &f_gen_25vf010;		/* u */
	dlist["fl_25vf010a"] = &f_gen_25vf010a;		/* t */
	dlist["fl_25..05"] = &f_gen_25x05;		/* u */
	dlist["fl_25..010"] = &f_gen_25x010;		/* u */
	dlist["fl_25..020"] = &f_gen_25x020;		/* u */
	dlist["fl_25..040"] = &f_gen_25x040;		/* u */
	dlist["fl_25..080"] = &f_gen_25x080;		/* u */
	dlist["fl_25..016"] = &f_gen_25x016;		/* u */
	dlist["fl_25..032"] = &f_gen_25x032;		/* u */
	dlist["fl_25..064"] = &f_gen_25x64;		/* u */
	dlist["fl_25..128"] = &f_gen_25x128;		/* t */
}

void packer::pack_data(unsigned char *data, int len, int plen)
{
	if (buff.size() < static_cast<size_t>(plen))
		buff.resize(plen);

	memset(&buff[0], 0, plen);
	memcpy(&buff[0], data, len);
}

void packer::pack_msg(int mesg, unsigned char *data, int len, int plen)
{
	if (buff.size() < static_cast<size_t>(plen + 1))
		buff.resize(plen + 1);

	buff[0] = mesg;

	memset(&buff[1], 0, plen);
	memcpy(&buff[1], data, len);
}

int protocol::setup_type(const string &type)
{
	if (dlist.find(type) == dlist.end()) {
		e << "device not available\nplease use the "
			"--list option for the supported device slist\n";
		return -1;
	}

	/* assign */
	current = dlist[type];

	return 0;
}

void protocol::setup_read_fw_ver()
{
	unsigned char cmd[2] = {msg_t_command, cmd_read_fw_ver};

	pack_data(cmd, 2, 32);
}

void protocol::setup_config()
{
	pack_msg(msg_t_config, (unsigned char *)current,
			sizeof(struct target_info), 33);
}

/* common to different spi nor, likely a pre-eread */
void protocol::setup_read()
{
	unsigned char cmd[2] = {msg_t_command, cmd_read_mem};

	pack_data(cmd, 2, 32);
}

void protocol::setup_sync()
{
	unsigned char cmd[2] = {msg_t_status, sta_t_synchropc};

	/* send me data now on ep_in_data */
	pack_data(cmd, 2, 33);
}

void protocol::setup_erase()
{
	unsigned char cmd[2] = {msg_t_command, cmd_clear_mem};

	pack_data(cmd, 2, 32);
}

void protocol::setup_write()
{
	unsigned char cmd[2] = {msg_t_command, cmd_write_mem};

	pack_data(cmd, 2, 32);
}

void protocol::setup_send_buff_start()
{
	unsigned char cmd[2] = {msg_t_command, cmd_send_buff_start};

	pack_data(cmd, 2, 32);
}

void protocol::setup_send_buff_stop()
{
	unsigned char cmd[2] = {msg_t_command, cmd_send_buff_stop};

	pack_data(cmd, 2, 32);
}

void protocol::setup_set_hw_vpp()
{
	unsigned char cmd[2] = {msg_t_command, cmd_set_hw_vpp};

	pack_data(cmd, 2, 32);
}

void protocol::setup_set_hw_bus()
{
	unsigned char cmd[2] = {msg_t_command, cmd_set_hw_bus};

	pack_data(cmd, 2, 32);
}
